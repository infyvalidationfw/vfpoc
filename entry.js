"use strict";
/*
  #%L
  Rules, Computation, Validation, Audit Microservice
  %%
  Copyright (C) 2020  In10S
  %%
  INTELLECTUAL PROPERTY RIGHTS.
  
   All titles and intellectual property rights in and to the "Product" 
   (including but not limited to any images, photographs, animations, 
   video, audio, music, text,source, "applets" etc. incorporated into the "Product"),
   and any copies you are permitted to make herein are owned by Intense Technologies Limted (IN10S).
   or its subsidiary. All titles and intellectual property rights in and to the 
   content which may be accessed through use of the "Product" is the 
   property of the respective content owner and may be protected by 
   applicable copyright or other intellectual property laws and treaties.  
   This "Agreement" grants you no rights to use such content. If this 
   "Product" contains documentation that is provided only in electronic form, 
   you may print one copy of such electronic documentation.  
   You may not copy the printed materials accompanying the "Product". 
  
  Please refer to the complete license.
  
            For inquiries please contact: 
              Intense Technologies LIMITED
              A1,Vikrampuri,Secunderabad-9.INDIA.
              Fax: 91-40-27819040
              E-mail:info@intense.in or info@In10stech.com
  #L%
  */
var express = require('express');
const bytenode = require('bytenode');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var fs = require('fs');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var appRoot = path.resolve(__dirname);
var routes = require('./routes/index');
var services = require('./routes/api');
var app = express();

var env = process.env.NODE_ENV || 'development';
var config = require(__dirname + '/environments/environment.js')[env];

var configTemplate = fs.readFileSync('./conf/configuration.json');
var _configTemplate = JSON.parse(configTemplate);

global.requestId = new Date().getTime();
var util = require('util');
if (config["LogsCofig"]["EnableLog"]) {
    var logpath = config["LogsCofig"]["PathTO_WriteLogs"];
    if (logpath == "") {
        logpath = "./logs"
            if (!fs.existsSync(logpath)) {
                fs.mkdirSync(logpath);
            }
    }

    var logFile = fs.createWriteStream(logpath + "/vf.log", {
        flags: 'a'
    });
    if (config["LogsCofig"]["LogLevel"].toUpperCase() == "INFO") {
        console.log = function () {
            logFile.write(requestId + "#:" + util.format.apply(null, arguments) + '\n');
        }
        console.error = function () {}
        console.debug = console.error;
    } else if (config["LogsCofig"]["LogLevel"].toUpperCase() == "ERROR") {
        console.error = function () {
            logFile.write(requestId + "#:" + util.format.apply(null, arguments) + '\n');
        }
        console.log = function () {}
        console.debug = console.log;
    } else if (config["LogsCofig"]["LogLevel"].toUpperCase() == "DEBUG") {
        console.debug = function () {
            logFile.write(requestId + "#:" + util.format.apply(null, arguments) + '\n');
        }
        console.log = console.debug;
        console.error = console.debug;
    }
} else {
    console.log = function () {}
    console.debug = console.log;
    console.error = console.log;
}
//var apiBase = config.Services.APIBASE || '/api';
var apiBase = '/';
var urlencodedsize = config.URLENCODESIZE || '10mb';

app.use(express.static(__dirname + '/html'));
//app.use(express.static(__dirname));
var pdmJs = require(__dirname + '/validations/js/pdm_info3.0.2');
var healthcheck = require('./routes/healthcheck.routes');
var rimraf = require("rimraf");
var getSize = require('get-folder-size');
//app.use(bodyParser.json({ limit: '50mb' }));
//app.use(bodyParser.urlencoded({ limit: '50mb', extended: false}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({
        limit: '10mb'
    }));
app.use(bodyParser.urlencoded({
        limit: urlencodedsize,
        extended: false
    }));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3400');
    res.setHeader('Access-Control-Allow-Methods', 'http://localhost:3400');

    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use('/api/health', healthcheck);
//need to optimize


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);
app.use('/results', routes);

app.get('/vReport', function (request, response) {
    var requestId = request.query.requestId;
    var itrType = request.query.itrType;
    let folderDate = request.query.currentDate;
    let folderTime = request.query.currentHour;
    let index = request.query.index;
    index = getFolderIndex(index,folderDate,folderTime);
    let fileName = requestId + "_" + itrType;
    let reportsPath = getReportsPath();
    console.log("requestId************" + reportsPath +"/" + folderDate + "/" + folderTime + "/" + fileName);
    let logInfo = null;
    if (requestId && fileName) {
        let jsonFile = reportsPath +"/" + folderDate + "/" + folderTime + index + "/" + fileName + ".json";
        logInfo = fs.readFileSync(jsonFile);
        console.log("requestId************" + requestId);
    }
    var dataToBind = {
        'logInfo': logInfo
    };
    response.writeHead(200, {
        "Content-Type": "text/html"
    });
    fs.readFile(__dirname + '/html/vReport.html', 'utf8', function (err, data) {
        if (err)
            throw err;
        data = doDataBinding(data, dataToBind);
        response.write(data);
        response.end();
    });
    //response.sendFile(__dirname+'/html/vReport.html');
});
function doDataBinding(data, databindings) {
    for (var prop in databindings)
        if (databindings.hasOwnProperty(prop))
            data = data.split('${' + prop + '}').join(databindings[prop]);
    return data;
}
app.get('/dashboard', function (request, response) {
    response.sendFile(__dirname + '/html/dashboard.html');
});
app.post("/getFilesList", function (req, res) {
    let currentDate = req.body.date;
    let hour = req.body.hour;
    getHourFolders(currentDate);
    var files = getFiles(__dirname + "/reports/" + currentDate + "/" + hour);
    //var n = Math.floor(Math.random() * files.length);
    res.send(files);
});

app.post("/getLatestFiles", function (req, res) {
    let currentDate = req.body.date;
    let hour = req.body.hour;
    res.send(getLatestFiles(currentDate, hour));
})
app.get("/getLatestReportsFolder", function (request, response) {
    response.send(getLatestFiles());
})
app.post("/getPdmTemplate", function (req, res) {
    try {
        let apiitr = req.body.itrtype;
        let assessmentYear = req.body.assessmentYear;
        let response = {};
        let rawData = getPdmTemplate(apiitr, assessmentYear);
        let constantData = getConstantsTemplate(apiitr, assessmentYear);
        response.pdmTemplate = rawData;
        response.constants = constantData;
        res.send(response);
        res.end();
    } catch (e) {
        res.status(503).json('failed');
    }
});
app.post("/updatePdmTemplate", function (req, res) {
    try {
        let apiitr = req.body.itrtype;
        let assessmentYear = req.body.assessmentYear;
        let template = req.body.template;
        let type = req.body.updateType;
        let filePath = '';

        if (type === 'pdm') {
            pdmJs.getPdmInfo(assessmentYear).setPdmTemplate(apiitr, undefined);
            filePath = getPdmFile(apiitr, assessmentYear);

        } else {
            filePath = getConstantFile(apiitr, assessmentYear);
        }

        fs.writeFileSync(filePath, template);
        //res.send(type);
        res.status(200).json('success');
    } catch (e) {
        res.status(503).json('failed');
    }
});
app.post("/updateConstants", function (req, res) {
    try {
        let apiitr = req.body.itrtype;
        let constantsTemplate = req.body.constants;
        //let assessmentYear = req.body.assessmentYear;
        //rawPdmTemplate = JSON.parse(rawPdmTemplate);
        let constantFilePath = getConstantFile(apiitr);
        fs.writeFileSync(constantFilePath, constantsTemplate);
        res.status(200).json('success');
    } catch (e) {
        res.status(503).json('failed');
    }
});
app.get('/ValidationUI', function (req, res) {
    let localHtmlContent = '';
    localHtmlContent = prepareHTMLContent();
    res.writeHeader(200, {
        'Content-Type': 'text/html'
    });
    //res.send(localHtmlContent);
    res.write(localHtmlContent);
    res.end();
});

app.post('/getHoursFolder', function (request, response) {
    let date = request.body.date;
    response.send(getHourFolders(date, ""));
});

function getPdmTemplate(apiitr, assessmentYear) {
    let pathOfPdmInfo = getPdmFile(apiitr, assessmentYear);
    let pdmInfoData = fs.readFileSync(pathOfPdmInfo);
    let rawData = JSON.parse(pdmInfoData);
    return rawData;
}
function getPdmFile(apiitr, assessmentYear) {
    let pdmsInfo = pdmJs.getPdmInfo(assessmentYear);
    let pathOfPdmInfo = pdmsInfo.getPDMFilePath(apiitr, true);
    console.log("pathOfPdmInfo pathOfPdmInfo  ", pathOfPdmInfo)
    return __dirname + "/" + pathOfPdmInfo;
}
function getConstantsTemplate(apiitr, assessmentYear) {
    let constantsPath = getConstantFile(apiitr, assessmentYear);
    let constantsData = fs.readFileSync(constantsPath);
    let rawData = JSON.parse(constantsData);
    return rawData;
}
function getConstantFile(apiitr, assessmentYear) {
    let constantsPath = __dirname + "/assets/json/sol_pdms_data/constants/" + apiitr + "_" + assessmentYear + "_all_constants.json";
    return constantsPath;
}
function prepareHTMLContent() {
    try {
        let localHtmlContent = '';
        let htmlContent = fs.readFileSync(__dirname + '/html/validation_framework.html', {
            encoding: 'utf8',
            flag: 'r'
        });
        let configTemplate = getEnvironmentConfig();
        let options = getItrOptions(configTemplate);
        localHtmlContent = htmlContent.replace("<!-- @@itr_options@@ -->", options);
        return localHtmlContent;
    } catch (e) {
        return e;
    }
}
function getEnvironmentConfig() {
    try {
        return config;
    } catch (e) {
        return e;
    }

}
function getItrOptions(configTemplate) {
    try {
        var itrsTemplate = configTemplate.Services.ITRS;
        var itrList = Object.keys(itrsTemplate);
        var options = "";
        for (var i = 0; i < itrList.length; i++) {
            options += "<option value=" + (i + 1) + ">" + itrList[i] + "</option>";
        }
        return options;
    } catch (e) {
        alert(e);
    }
    return localHtmlContent;
}
function getFiles(dir) {
    let files_ = [];
    var files = fs.readdirSync(dir);
    files.sort(function (b, a) {
        return fs.statSync(dir + "/" + a).mtime.getTime() -
        fs.statSync(dir + "/" + b).mtime.getTime();
    });
    let pageCount = 1;
    for (var i in files) {
        let noOfReportsTOShow = config["AuditLogs"]["NoOfReportsToShow"];
        if (pageCount > noOfReportsTOShow)
            break;
        var name = files[i];
        let arr = name.split("_");
        console.log("arrarrarrarr ::::: " + arr);
        let obj = {
            "time": formatDate(new Date(+arr[0]), 'EEEE, MMMM d, yyyy HH:mm:ss.S aaa'),
            "itrType": arr[2],
            "requestId": arr[0] + "_" + arr[1]
        };
        files_.push(obj);
        pageCount++;

    }
    return files_;
}

function getHourFolders(folder, hours) {
    let reportsPath = getReportsPath();
    let dateFolder = reportsPath + "/" + folder;
    let files = [];
    let hourFiles = [];
    try {
        files = fs.readdirSync(dateFolder);
        if (hours != "") {
            for (let file of files) {
                if (file === hours || file.startsWith(hours + "_"))
                    hourFiles.push(file);
            }
            files = specialSortArray(hourFiles);
        } else {
            for (let file of files) {
                if (file.includes("_"))
                    continue;
                hourFiles.push(file);
            }
            files = hourFiles.sort();
        }
    } catch (e) {
        console.log(e);
    }
    return files;
}
function specialSortArray(array) {
    
    let hourNumber = array[0];
    array.splice(0,1);
	array.splice(array.length,0,hourNumber);
    
    return array;
}
function getLatestFiles(date, hourNumber) {
    if (date == "")
        date = getLatestDate();
    if (hourNumber == "") {
        let hoursList = getHourFolders(date, "");
        hourNumber = hoursList[hoursList.length - 1];
    }
    let hours = getHourFolders(date, hourNumber + "");
    let latestFiles = [];
    let reportsPath = getReportsPath();
    for (let hour of hours) {
        let files = getFiles(reportsPath+"/" + date + "/" + hour);
        for (let file of files) {
            latestFiles.push(file);
        };
    }
    let outputData = {};
    outputData.date = date;
    outputData.hour = hourNumber;
    outputData.tableData = latestFiles;
    return outputData;
}
function getLatestDate() {
    let reportsPath = getReportsPath();
    let datesFolder = reportsPath;
    let files = [];
    let latestFolder = "";
    try {
        files = fs.readdirSync(datesFolder);
        let comparedFile = files[0];
        let dad = comparedFile.split("-");
        comparedFile = comparedFile.replace(dad[1], dad[0]);
        comparedFile = comparedFile.replace(dad[0], dad[1]);
        comparedDate = Date.parse(comparedFile);
        latestFolder = files[0];
        for (let index = 1; index < files.length; index++) {
            let comparatorFile = files[index];
            let cad = comparatorFile.split("-");
            comparatorFile = comparatorFile.replace(cad[1], cad[0]);
            comparatorFile = comparatorFile.replace(cad[0], cad[1]);
            let comparatorDate = Date.parse(comparatorFile);
            if (comparatorDate > comparedDate) {
                latestFolder = files[index];
                comparedDate = comparatorDate;
            }

        }
    } catch (e) {
        console.log(e);
    }
    return latestFolder;
}

function getFolderIndex(index,date,hour){   
    let filesPerFolder = config["AuditLogs"]["FilesInDir"];
    let hoursArray = getHourFolders(date,hour); 
    let number = index / filesPerFolder ;
    if(number % 1 != 0){
        number = Math.trunc(number);
        number++;
    }    
    if(hoursArray.includes(hour+"_" + number))       
        return "_" + number;
    else
        return "";   
}

function getReportsPath(){
    let reportsPath = config["AuditLogs"]["PathTO_Generate_Reports"];
    if(reportsPath === "")
        return __dirname + "/" + "reports";
    
    return reportsPath;    
}
app.use(apiBase, services);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: (app.get('env') === 'development') ? err : {}
    });
});

var monthNames = [
    "January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"
];
var dayOfWeekNames = [
    "Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday", "Saturday"
];
function formatDate(date, patternStr) {
    if (!patternStr) {
        patternStr = 'M/d/yyyy';
    }
    var day = date.getDate(),
    month = date.getMonth(),
    year = date.getFullYear(),
    hour = date.getHours(),
    minute = date.getMinutes(),
    second = date.getSeconds(),
    miliseconds = date.getMilliseconds(),
    h = hour % 12,
    hh = twoDigitPad(h),
    HH = twoDigitPad(hour),
    mm = twoDigitPad(minute),
    ss = twoDigitPad(second),
    aaa = hour < 12 ? 'AM' : 'PM',
    EEEE = dayOfWeekNames[date.getDay()],
    EEE = EEEE.substr(0, 3),
    dd = twoDigitPad(day),
    M = month + 1,
    MM = twoDigitPad(M),
    MMMM = monthNames[month],
    MMM = MMMM.substr(0, 3),
    yyyy = year + "",
    yy = yyyy.substr(2, 2);
    // checks to see if month name will be used
    patternStr = patternStr
        .replace('hh', hh).replace('h', h)
        .replace('HH', HH).replace('H', hour)
        .replace('mm', mm).replace('m', minute)
        .replace('ss', ss).replace('s', second)
        .replace('S', miliseconds)
        .replace('dd', dd).replace('d', day)

        .replace('EEEE', EEEE).replace('EEE', EEE)
        .replace('yyyy', yyyy)
        .replace('yy', yy)
        .replace('aaa', aaa);
    if (patternStr.indexOf('MMM') > -1) {
        patternStr = patternStr
            .replace('MMMM', MMMM)
            .replace('MMM', MMM);
    } else {
        patternStr = patternStr
            .replace('MM', MM)
            .replace('M', M);
    }
    return patternStr;
}
function twoDigitPad(num) {
    return num < 10 ? "0" + num : num;
}

try {
    if (config['AuditLogs']["Generate_AuditLogs"]) {
        setInterval(function () {
            getSize(__dirname + '/reports', (err, size) => {
                try {
                    if (err) {
                        throw err;
                    }
                    var sizeInMb = (size / 1024 / 1024).toFixed(2);
                    deleteHistory(config["AuditLogs"]["Delete_ReportsHistory_By_Hours"]);

                    if (sizeInMb > 5120) {
                        process.stdout.write("\x1b[91mDirectory size exceeds 5gb please clear the reports history or change the configuration days to hours for clear history \n \x1b[39m");
                    }
                    /*	if(sizeInMb>config["Delete_History_By_ExceedSpace_in_MB"]){
                    deleteHistory(config["Days_OR_Hours"],"hours");
                    }else{
                    deleteHistory(config["Days_OR_Hours"],config["Delete_History_By_Days_OR_Hours"]);
                    }	*/
                } catch (e) {
                    console.error(e);
                }
            });

        }, config["AuditLogs"]["Calculate_ReportsHistory_By_Given_Minutes"] * 60 * 1000);
    }
} catch (e) {
    console.error(e);
}
function deleteHistory(limit) {
    var uploadsDir = __dirname + '/reports';
    fs.readdir(uploadsDir, function (err, files) {
        files.forEach(function (file, index) {
            fs.stat(path.join(uploadsDir, file), function (err, stat) {
                var endTime,
                now;
                if (err) {
                    return console.error(err);
                }
                now = new Date().getTime();
                endTime = new Date(stat.ctime).getTime() + 1000 * 60 * 60 * limit;
                if (now > endTime) {
                    return rimraf(path.join(uploadsDir, file), function (err) {
                        if (err) {
                            return console.error(err);
                        }
                        process.stdout.write("\x1b[33m History cleared successfully \x1b[89m");
                    });
                }
            });
        });
    });
}


    try {
		if (_configTemplate && _configTemplate['disableRules']) {
			let disableRules_info = _configTemplate['disableRules_info'];
			for (let itrType in disableRules_info) {
				let itr_info = disableRules_info[itrType];
				for (let year in itr_info) {
					let i18nkeyArray = itr_info[year];
					let fileName = getFileName(itrType, year);
					var pdmContent=null;
					pdmContent=fs.readFileSync("./assets/json/sol_pdms_data/" + fileName,'utf8');
					var pdmInfoTemplate = JSON.parse(pdmContent);
					for (let i18nKey of i18nkeyArray) {
						let entity_Field = i18nKey.substring(i18nKey.lastIndexOf(".") + 1, i18nKey.lastIndexOf("_"));
						if (pdmInfoTemplate.pdmTemplate[entity_Field]) {
							disableRules(pdmInfoTemplate.pdmTemplate[entity_Field], i18nKey);
						}
					}
					if (pdmInfoTemplate) {
						fs.writeFileSync("./assets/json/sol_pdms_data/" + fileName, JSON.stringify(pdmInfoTemplate, null, 4));
					}
				}
			}
		}
    } catch (e) {
        console.error(e);
    }


function getFileName(itrtype, assyear) {
    var files = fs.readdirSync("./assets/json/sol_pdms_data");
    for (let file in files) {
        if (files[file].includes(itrtype)) {
            return files[file]
        }
    }
}

function disableRules(fieldObj, i18nkey) {
    let validations = fieldObj["validations"];
    if (Array.isArray(validations)) {
        for (let validation of validations) {
            if (validation['i18nkey'] == i18nkey) {
                validation['doUploadValidation'] = false;
                return;
            }
        }
    }
    let validationRules = fieldObj["validationRules"];
    if (Array.isArray(validationRules)) {
        for (let validationRule of validationRules) {
            let validations = validationRule["validations"];
            if (Array.isArray(validations)) {
                for (let validation of validations) {
                    if(validation['i18nkey'] == i18nkey) {
                        validation['doUploadValidation'] = false;
                    }
                }
            }
        }
    }
}

module.exports = app;
