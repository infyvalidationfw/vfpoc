var validations  = require('../validations');
var express = require('express');
var router  = express.Router();
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../environments/environment.js')[env];
var vtitle = config.TITLE || 'FOReturnsValidation' ;

router.get('/', function(req, res) {
    res.render('index', {
      title: vtitle 
    });
});

router.get('/results', function(req, res) {
    res.render('result', {
      title: vtitle
    });
});
module.exports = router;
