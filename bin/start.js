#!/usr/bin/env node
/*
  #%L
  Rules, Computation, Validation, Audit Microservice
  %%
  Copyright (C) 2020  In10S
  %%
  INTELLECTUAL PROPERTY RIGHTS.
  
   All titles and intellectual property rights in and to the "Product" 
   (including but not limited to any images, photographs, animations, 
   video, audio, music, text,source, "applets" etc. incorporated into the "Product"),
   and any copies you are permitted to make herein are owned by Intense Technologies Limted (IN10S).
   or its subsidiary. All titles and intellectual property rights in and to the 
   content which may be accessed through use of the "Product" is the 
   property of the respective content owner and may be protected by 
   applicable copyright or other intellectual property laws and treaties.  
   This "Agreement" grants you no rights to use such content. If this 
   "Product" contains documentation that is provided only in electronic form, 
   you may print one copy of such electronic documentation.  
   You may not copy the printed materials accompanying the "Product". 
  
  Please refer to the complete license.
  
            For inquiries please contact: 
              Intense Technologies LIMITED
              A1,Vikrampuri,Secunderabad-9.INDIA.
              Fax: 91-40-27819040
              E-mail:info@intense.in or info@In10stech.com
  #L%
  */

/**
 * Module dependencies.
 */
var app = require('../entry');
var debug = require('debug')('validationfw-');
var http = require('http');
var validations = require('../validations');
var env = process.env.NODE_ENV || 'development';
var config = require(__dirname + '/../environments/environment.js')[env];
var ipadd = config.ipadd || '0.0.0.0';

var fs = require('fs');




/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(config.PORT || '3000');
app.set('port', port);
/**
 * Create HTTP server.
 */

if (config["ClusterConfig"]["EnableCluster"]) {
    var cluster = require('cluster');
    if (cluster.isMaster) {
        var numWorkers = require('os').cpus().length;
        let coresToUse = 1;
        let minCoresToUse = config["ClusterConfig"]["MinCoresToUse"];
        let maxCoresToUse = config["ClusterConfig"]["MaxCoresToUse"];
        if (minCoresToUse <= numWorkers) {
            if (maxCoresToUse >= numWorkers) {
                coresToUse = numWorkers;
            } else {
                coresToUse = maxCoresToUse
            }
        }
        //numWorkers = numWorkers - _configTemplate["NoofCoresToLeave"] || 1;
        console.log('Master cluster setting up ' + coresToUse + ' workers...');
        for (var i = 0; i < coresToUse; i++) {
            cluster.fork();
        }
        cluster.on('online', function (worker) {
            console.log('Worker ' + worker.process.pid + ' is online');
        });
        cluster.on('exit', function (worker, code, signal) {
            console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
            console.log('Starting a new worker');
            cluster.fork();
        });
    } else {
        var server = app.listen(port, function () {
            debug('Process ' + process.pid + ' is listening to all incoming requests');
        });
        server.on('error', onError);
        server.on('listening', function () {
            onListening(server)
        });
    }
} else {
    var server = http.createServer(app);
    server.listen(port, ipadd, function () {
        debug('Express server listening on port ' + server.address().port);
    });
    server.on('error', onError);
    server.on('listening', function () {
        onListening(server)
    });
}



/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
         ? 'Pipe ' + port
         : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
    case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
    case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
    default:
        throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening(server) {
    var addr = server.address();
    var bind = typeof addr === 'string'
         ? 'pipe ' + addr
         : 'port ' + addr.port;
    debug('Listening on ' + bind);
}
