module.exports = {
  "development" : {
    "TITLE" : "FOReturnsValidation",
    "VERSION" : "0.1.4",
    "PORT" : "2025",
    "ENCODESIZE" : "5mb",
    "PDMVERSION" : "3.0.2",
    "loggername" : "dev",
    "XMLLimit" : "1mb",
    "normalize" : true,
    "normalizeTags" : false,
    "explicitArray" : false,
    "validateUploadLevelOnly" : true,
    "ipadd" : "0.0.0.0",
    "ClusterConfig" : {
      "EnableCluster" : true,
      "MinCoresToUse" : 1,
      "MaxCoresToUse" : 4
    },
    "LogsCofig" : {
      "EnableLog" : false,
      "PathTO_WriteLogs" : "",
      "LogLevel" : "error",
      "logMaxSizeinMb" : 50,
      "availableLogLevels" : [ "error", "info", "debug" ]
    },
    "AuditLogs" : {
      "Generate_AuditLogs" : false,
      "PathTO_Generate_Reports" : "",
      "FilesInDir" : 1000,
      "Calculate_ReportsHistory_By_Given_Minutes" : 5,
      "Delete_ReportsHistory_By_Hours" : 48,
      "NoOfReportsToShow" : 10
    },
    "Services" : {
      "APIBASE" : "/api",
      "singleAPI" : true,
      "singleVersion" : "1.0.0",
      "ITRS" : {
        "ITR1" : {
          "Name" : "itr1",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr1",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr1",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr1",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr1",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr1",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr1",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR2" : {
          "Name" : "itr2",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr2",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr2",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr2",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr2",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr2",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr2",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR3" : {
          "Name" : "itr3",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr3",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr3",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr3",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr3",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr3",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr3",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR4" : {
          "Name" : "itr4",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr4",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr4",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr4",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr4",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr4",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr4",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR5" : {
          "Name" : "itr5",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr5",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr5",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr5",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr5",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr5",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr5",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR6" : {
          "Name" : "itr6",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr6",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr6",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr6",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr6",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr6",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr6",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        },
        "ITR7" : {
          "Name" : "itr7",
          "APIVersions" : {
            "VERSIONS" : [ {
              "VERSION" : "1.0.0",
              "TEMPLATE" : "uploaditr7",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr7",
              "PDM" : "pdmvalidations1.0.0.js",
              "SCHEMA" : "schema_itr7",
              "ISSCHEMAVALID" : false
            }, {
              "VERSION" : "1.0.1",
              "TEMPLATE" : "uploaditr7",
              "MSGTEMPLATE" : "UploadValidationMsgs_itr7",
              "PDM" : "vfAll3.0.2.js",
              "SCHEMA" : "schema_itr7",
              "ISSCHEMAVALID" : false,
              "LATEST" : true
            } ]
          }
        }
      }
    },
    "PAN_pathOf_itr1" : "ITR.ITR1.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr1" : "ITR.ITR1.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr2" : "ITR.ITR2.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr2" : "ITR.ITR2.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr3" : "ITR.ITR3.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr3" : "ITR.ITR3.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr4" : "ITR.ITR4.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr4" : "ITR.ITR4.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr5" : "ITR.ITR5.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr5" : "ITR.ITR5.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr6" : "ITR.ITR6.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr6" : "ITR.ITR6.PersonalInfo.AadhaarCardNo",
    "PAN_pathOf_itr7" : "ITR.ITR7.PersonalInfo.PAN",
    "AadhaarCardNo_pathOf_itr7" : "ITR.ITR7.PersonalInfo.AadhaarCardNo"
  },
  "test" : { },
  "production" : { }
};